package com.english.finding.be.data

import com.minhphu.basemodule.utils.CipherHelper
import com.vicpin.krealmextensions.query
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Test : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var catId: Long? = null
    var question: String? = null
    private var optionA: String? = null
    private var optionB: String? = null
    private var optionC: String? = null
    private var optionD: String? = null
    private var ans: String? = null

    var userAns: String? = null

    fun question(): String {
        return CipherHelper.decrypt(question!!)
    }

    private fun optionA(): String {
        return optionA!!
    }

    private fun optionB(): String {
        return optionB!!
    }

    private fun optionC(): String {
        return optionC!!
    }

    private fun optionD(): String {
        return optionD!!
    }

    fun getAnswers(): String {
        when (ans) {
            "A" -> return optionA()
            "B" -> return optionB()
            "C" -> return optionC()
            "D" -> return optionD()
        }
        return ""
    }


    fun getRandomAns(): ArrayList<String> {
        val randList = ArrayList<String>(arrayListOf(optionA(), optionB(), optionC()))
        if (optionD !=null && optionD?.length!! > 0) {
            randList.add(optionD())
        }
        randList.shuffle()
        return randList
    }

    companion object {

        fun getAllByCat(catId: Int): List<Test> {
            return Test().query { equalTo("catId", catId) }
        }

    }

}