package com.english.finding.be.ui.main

import com.english.finding.be.R
import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.vending.billing.IInAppBillingService
import com.english.finding.be.utils.PreferenceUtils
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.navigation.NavigationView
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.minhphu.basemodule.dialog.RatingDialog
import com.minhphu.basemodule.promoteapp.AppInfo
import com.minhphu.basemodule.promoteapp.PromoteAppAdapter
import com.minhphu.basemodule.purchase.IabHelper
import com.minhphu.basemodule.purchase.IabResult
import com.minhphu.basemodule.purchase.Purchase
import com.minhphu.basemodule.utils.ConnectionUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_category.view.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, IabHelper.OnIabPurchaseFinishedListener, IabHelper.OnIabSetupFinishedListener {

    companion object {
        const val RESULT_SHOW_ADS = 1000
        const val COUNT_STORY_ADS = 2
        const val LEVEL_1 = "beginner"
        const val LEVEL_2 = "intermediate"
        const val LEVEL_3 = "advance"

        const val KEY_PROMOTE = "finding_error_menu"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this, getString(R.string.app_ads_id))
        restorePurchase()

        setUpDrawer()
        loadNewCategory(LEVEL_1)

        btnChangeCategory.setOnClickListener {
            val bottomSheetDialog = BottomSheetDialog(this)
            val sheetView = layoutInflater.inflate(R.layout.bottom_sheet_category, null)

            sheetView.btnBgn.setOnClickListener {
                loadNewCategory(LEVEL_1)
                bottomSheetDialog.dismiss()
            }

            sheetView.btnInter.setOnClickListener {
                loadNewCategory(LEVEL_2)
                bottomSheetDialog.dismiss()
            }

            sheetView.btnAdv.setOnClickListener {
                loadNewCategory(LEVEL_3)
                bottomSheetDialog.dismiss()
            }

            bottomSheetDialog.setContentView(sheetView)
            bottomSheetDialog.show()
        }
    }

    private fun loadNewCategory(level: String) {
        txtLevel.text = level.toUpperCase()
        val transaction = supportFragmentManager.beginTransaction()
        val categoryFragment = ListTestFragment.newInstance(level)
        transaction.replace(R.id.mainContainer, categoryFragment)
        transaction.commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_remove_ads -> {
                if (ConnectionUtils.isNetworkConnected(this)) {
                    startPurchase()
                }
            }
            R.id.action_share_app -> {
                val shareBody = "https://play.google.com/store/apps/details?id=$packageName"
                val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.app_name)))
            }
            R.id.action_rate -> openRate()

        }
        return true
    }

    private fun setUpDrawer() {
        val toggle = ActionBarDrawerToggle(this, drawerMain, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerMain.addDrawerListener(toggle)
        toggle.syncState()

        setUpPromoteApp()

        navigationMain.setNavigationItemSelectedListener(this)
    }

    private fun setUpPromoteApp() {
        val menuJsonString = FirebaseRemoteConfig.getInstance().getString(KEY_PROMOTE)
        val v = navigationMain.inflateHeaderView(R.layout.more_app)
        val btnDevPage = v.findViewById<LinearLayout>(R.id.btnDevPage)
        if (!menuJsonString.isNullOrEmpty()) {
            val rcvPromoteApp = v.findViewById<RecyclerView>(R.id.rcvPromoteApp)
            val jsonPromoteApp = JSONArray(menuJsonString)
            val listAppInfo = ArrayList<AppInfo>()
            for (i: Int in 0 until jsonPromoteApp.length()) {
                val jsonObjectPromoteApp: JSONObject = jsonPromoteApp.getJSONObject(i)
                val appName = jsonObjectPromoteApp.getString("app_name")
                val packageName = jsonObjectPromoteApp.getString("package_name")
                val keyIcon = packageName.replace(".", "")
                val base64Image = FirebaseRemoteConfig.getInstance().getString(keyIcon)
                listAppInfo.add(AppInfo(appName, packageName, base64Image))
            }
            rcvPromoteApp.adapter = PromoteAppAdapter(listAppInfo, object : PromoteAppAdapter.ListAppMenuCallback {
                override fun onClick(menuUI: AppInfo) {
                    openPromo(menuUI.packageName)
                }
            })
        }
        btnDevPage.findViewById<LinearLayout>(R.id.btnDevPage).setOnClickListener {
            if (ConnectionUtils.isNetworkConnected(this)) {
                val devIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.dev_page)))
                devIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(devIntent)
            }
        }
    }

    private fun openPromo(url: String) {
        if (ConnectionUtils.isNetworkConnected(this)) {
            val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$url"))
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(marketIntent)
        } else {
            Toast.makeText(this@MainActivity, getString(R.string.txt_check_internet), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPressed() {
        when {
            drawerMain.isDrawerOpen(GravityCompat.START) -> drawerMain.closeDrawer(GravityCompat.START)
            !PreferenceUtils.checkUserRateApp() -> {
                val ratingDialog = RatingDialog.getDialog(this, onYesClick = {
                    val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${packageName}"))
                    marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(marketIntent)
                    PreferenceUtils.updateIsRated(this)
                }, onNoClick = {
                    finish()
                })
                ratingDialog.show()
            }
            else -> super.onBackPressed()
        }
    }

    public override fun onDestroy() {
        if (mService != null) {
            unbindService(mServiceConn)
        }
        super.onDestroy()
    }

    private fun openRate() {
        if (ConnectionUtils.isNetworkConnected(this)) {
            try {
                val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
                marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(marketIntent)
            } catch (e: Exception) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }
            PreferenceUtils.updateIsRated(this)

        } else {
            Toast.makeText(this@MainActivity, "Please check Internet", Toast.LENGTH_SHORT).show()
        }
    }

    private var mHelper: IabHelper? = null
    var mService: IInAppBillingService? = null
    private val skuRemoveAds = "com.english.finding.be.removeads"
    private var base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsiGXbGE6wynZaULw7NQA8oNgVDWKS0F2H3ENJZ/3daSllJzSHWt504svZ9R2UspK5VRJzgSoaxEQqTBNtaolcC4B8UpWNj9O5A/57toADofnxItzr2bGzBAivTTst7/hrLedP3x8slTTGZlo50wLgELkW0094oHxM9V5Od4uRQL9D7q9j/J/4GZ21tlJ8F7RceJoSUUmLhEELboJCWZBzKLYoUA5i0EN0P+1aOeltVdH54jFUksUfTyikLCbYMFEyvF4yzyusiMA/eysgV9xCrM8bdDUc/BfYh4eMshRmhfXjQJvd4SJX5ZyndCV+yycD5vHWsiZR2DTUz1GbWyhsQIDAQAB"

    private fun restorePurchase() {
        if (!PreferenceUtils.checkPurchaseAd(this@MainActivity)) {
            navigationMain.menu.findItem(R.id.action_remove_ads).isVisible = true
            val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
            serviceIntent.`package` = "com.android.vending"
            bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE)
        }
    }

    private var mServiceConn: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mService = IInAppBillingService.Stub.asInterface(service)
            val ownedItems: Bundle
            try {
                ownedItems = mService!!.getPurchases(3, packageName, "inapp", null)
                val response = ownedItems.getInt("RESPONSE_CODE")
                if (response != 0) {
                    return
                }

                val ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST")
                if (ownedSkus != null) {
                    (0 until ownedSkus.size)
                            .filter { ownedSkus[it] == skuRemoveAds }
                            .forEach { successPurchaseRemoveAds() }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun successPurchaseRemoveAds() {
        PreferenceUtils.updatePurchaseAd(this, true)
        val builder1 = AlertDialog.Builder(this@MainActivity)
        builder1.setTitle(getString(R.string.title_remove_ads))
        builder1.setMessage(getString(R.string.mess_remove_ads))
        builder1.setCancelable(false)
        builder1.setPositiveButton(getString(R.string.ok)) { _, _ -> finish() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun startPurchase() {
        mHelper = IabHelper(this@MainActivity, base64EncodedPublicKey)
        mHelper?.startSetup(this@MainActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (mHelper != null) {
            if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onIabSetupFinished(result: IabResult) {
        if (result.isSuccess) {
            try {
                mHelper?.launchPurchaseFlow(this@MainActivity, skuRemoveAds, 10001, this@MainActivity)
            } catch (e: IabHelper.IabAsyncInProgressException) {
                e.printStackTrace()
            }
        }
    }

    override fun onIabPurchaseFinished(result: IabResult, purchase: Purchase?) {
        if (mHelper == null) {
            return
        }
        val responseCode: Int = result.response
        if (result.isFailure && responseCode != 7) {
            return
        }
        if (responseCode == 7 || purchase!!.sku == skuRemoveAds) {
            successPurchaseRemoveAds()
        }
    }

}
