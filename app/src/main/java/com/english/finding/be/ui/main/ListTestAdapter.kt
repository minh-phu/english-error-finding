package com.english.finding.be.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.english.finding.be.R
import com.english.finding.be.R.layout.item_category
import com.english.finding.be.data.Category
import com.english.finding.be.utils.PreferenceUtils
import com.minhphu.basemodule.ads.AdUtil
import kotlinx.android.synthetic.main.item_category.view.*

class ListTestAdapter(private val context: Context, private var categoryList: List<Category>) : androidx.recyclerview.widget.RecyclerView.Adapter<ListTestAdapter.ViewHolder>() {
    companion object {
        const val ITEM_SHOW_ADS = 8
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(item_category, viewGroup, false))
    }

    override fun onBindViewHolder(itemViewHolder: ViewHolder, position: Int) {
        val category = categoryList[position]
        itemViewHolder.itemView.txtName.text = category.name
        itemViewHolder.itemView.txtProgress.text = context.getString(R.string.txt_progress, category.score, 10)
        itemViewHolder.itemView.questionProgress.max = category.getTotalTestNumber()
        itemViewHolder.itemView.questionProgress.progress = category.score!!.toInt()

        try {
            if (position % ITEM_SHOW_ADS == 0) {
                val layoutNativeAds = itemViewHolder.itemView.findViewById<androidx.cardview.widget.CardView>(R.id.adsContainer)
                if (layoutNativeAds.childCount == 0) {
                    if (!PreferenceUtils.checkPurchaseAd(context)) {
                        AdUtil.showNativeAdsFbSmallInList(context, layoutNativeAds, context.getString(R.string.adsFb_ListTestFragment),context.getString(R.string.adsGg_ListTestFragment))
                    }
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun getItemViewType(position: Int): Int = position

    override fun getItemCount(): Int = categoryList.size

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        init {
            itemView.btnItem.setOnClickListener {
                categoryEvent?.onItemCategoryClick(categoryList[adapterPosition].id.toInt())
            }
        }
    }

    var categoryEvent: ListCategoryView? = null

    interface ListCategoryView {
        fun onItemCategoryClick(catId: Int)
    }

}
